package com.example.helloworld;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
public class HelloWorldController {
    @Value("${custom.text}")
    private String customText;

    @GetMapping("/hello")
    public ResponseEntity<String> getHelloWorld() {
        return ResponseEntity.ok("Hello World!");
    }

    @GetMapping("/custom")
    public ResponseEntity<String> getCustomText() {
        return ResponseEntity.ok(customText);
    }
}
