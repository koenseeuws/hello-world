FROM eclipse-temurin:17-jre-alpine
ADD /build/libs/*SNAPSHOT.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]